<?php
    require_once('animal.php');
    require_once('animal2.php');
    require_once('animal3.php');

    $hewan = new Animal("shaun");
    echo "Name : ".$hewan->nama_hewan."<br>";
    echo "legs : ".$hewan->jumlah_Kaki."<br>";
    echo "cold blooded : ".$hewan->cold_blooded."<br><br>";

    $hewan2 = new Animal2("buduk");
    echo "Name : ".$hewan2->nama_hewan."<br>";
    echo "legs : ".$hewan2->jumlah_Kaki."<br>";
    echo "cold blooded : ".$hewan2->cold_blooded."<br>";
    echo $hewan2->jump("Hop Hop")."<br><br>";

    $hewan3 = new Animal3("kera sakti");
    echo "Name : ".$hewan3->nama_hewan."<br>";
    echo "legs : ".$hewan3->jumlah_Kaki."<br>";
    echo "cold blooded : ".$hewan3->cold_blooded."<br>";
    echo $hewan3->yell("Auooo")."<br>";
?>